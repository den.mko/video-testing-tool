import { useState } from "react";
import { EnterDialog } from "./components/html/enter";
import { SessionInfo } from "./components/html/info";
import { DemoVRSDGPage } from "./sdg/demo";

function App() {
  const [state, setState] = useState<"hide" | "enter-dialog" | "VR" | "AR">(
    "enter-dialog",
  );

  return (
    <>
      {state === "enter-dialog" && <EnterDialog setState={setState} />}
      {(state === "VR" || state === "AR") && (
        <SessionInfo type={state} setState={setState} />
      )}
      <DemoVRSDGPage isStereoByDefault={true} />
    </>
  );

  return (
    <>
      {state === "enter-dialog" && <EnterDialog setState={setState} />}
      {(state === "VR" || state === "AR") && (
        <SessionInfo type={state} setState={setState} />
      )}
      {/* <XRCanvas
        dpr={window.devicePixelRatio}
        gl={{ localClippingEnabled: true }}
        frameBufferScaling={frameBufferScaling}
        frameRate={heighestAvailableFramerate}
      >
        <directionalLight position={[-2, 2, 2]} />
        <MusicPlayer />
        <SessionModeGuard deny="immersive-ar">
          <Suspense>
            <Environment
              blur={0.05}
              files="skybox/apartment_4k.hdr"
              background
            />
          </Suspense>
        </SessionModeGuard>
        <ImmersiveSession />
        <NonImmersiveCamera position={[0, 1.5, 0.5]} />
      </XRCanvas> */}
    </>
  );
}

export default App;
