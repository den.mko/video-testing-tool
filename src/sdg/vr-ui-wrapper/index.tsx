import { FontFamilyProvider, RootContainer } from "@coconut-xr/koestlich";
import type { FontFamilies } from "@coconut-xr/koestlich/dist/components/text";
import type { ComponentProps } from "react";
import { memo, Suspense } from "react";

type Props = ComponentProps<typeof RootContainer>;

const fontFamilies: FontFamilies = {
  regular: ["/fonts/opensans/regular/", "font.json"], // 400
  semibold: ["/fonts/opensans/semibold/", "font.json"], // 600
  extrabold: ["/fonts/opensans/extrabold/", "font.json"], // 800
};

export const VRUIWrapper = memo((props: Props) => {
  return (
    <FontFamilyProvider
      fontFamilies={fontFamilies}
      defaultFontFamily={"regular"}
    >
      <Suspense>
        <RootContainer
          flexDirection={"column"}
          padding={8}
          pixelSize={0.005}
          {...props}
        >
          {props.children}
        </RootContainer>
      </Suspense>
    </FontFamilyProvider>
  );
});
