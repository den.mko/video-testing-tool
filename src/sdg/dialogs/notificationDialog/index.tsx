import {memo} from 'react';
import {Text} from '@coconut-xr/koestlich';
import Style from './style';
import {VRDialogWindow} from '../../commons/dialog';

type Props = {
  text: string;
  onClose?: () => void;
};

export const VRNotificationDialog = memo((props: Props) => {
  const {text, onClose} = props;

  return (
    <VRDialogWindow
      type={'notification'}
      closeButtonType={'close'}
      withCloseButton={true}
      onClose={onClose}
      onClick={onClose}
    >
      <Text classes={Style.label}>{text}</Text>
    </VRDialogWindow>
  );
});
