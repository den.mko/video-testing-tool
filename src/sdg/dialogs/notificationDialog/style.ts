import {TextStyle} from '../../commons/vr-utils/vrClassNames';
import theme from '../../theme';

const Style: {
  label: TextStyle;
} = {
  label: [
    {
      color: theme.lightColor,
      fontSize: 20,
    },
  ],
};
export default Style;
