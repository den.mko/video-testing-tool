import { Container, Text } from "@coconut-xr/koestlich";
import { memo, useState } from "react";
import { Vector3 } from "three";
import { videoSlots } from "../../../video-slots.config";
import { VRButton } from "../../commons/button";
import { VRDialogWindow } from "../../commons/dialog";
// const videos = import.meta.glob("/*.(mp4)", { eager: true, import: "default" });

type Props = {
  changeHFOV: (step: number) => void;
  changeVFOV: (step: number) => void;
  setHFOV: (step: number) => void;
  setVFOV: (step: number) => void;
  changeRadius: (step: number) => void;
  toggleStereo: () => void;
  translate: (offset: Vector3) => void;
  setVideo: (link: string) => void;
  isStereo: boolean;
  hfov: number;
  vfov: number;
  radius: number;
};

type VideoPreset = {
  vfov: number;
  hfov: number;
  href: string;
  name: string;
};

const videoPresets: VideoPreset[] = [
  {
    href: "presets/video (kandao).mp4",
    vfov: 52,
    hfov: 66,
    name: "Kandao",
  },
  {
    href: "presets/video (candidate 1).mp4",
    vfov: 180,
    hfov: 180,
    name: "Candidate 1",
  },
  {
    href: "presets/video (candidate 2).mp4",
    vfov: 67.5,
    hfov: 120,
    name: "Candidate 2",
  },
  {
    href: "presets/video (mary).mp4",
    vfov: 180,
    hfov: 360,
    name: "360 cartoon",
  },
];

export const FOVChangeDialog = memo((props: Props) => {
  const {
    changeHFOV,
    changeVFOV,
    setHFOV,
    setVFOV,
    toggleStereo,
    changeRadius,
    translate,
    setVideo,
    radius,
    isStereo,
    hfov,
    vfov,
  } = props;

  const [step, setStep] = useState(10);
  const [translationStep] = useState(0.1);

  const [isShowVideosSection, setIsShowVideosSection] = useState(false);
  const [isShowVideosSlots, setIsShowVideosSlots] = useState(false);
  const [isShowTranslateSection, setIsShowTranslateSection] = useState(false);
  const [isShowScreenSettingSection, setIsShowScreenSettingSection] =
    useState(false);

  return (
    <VRDialogWindow type={"primary"} gapRow={10}>
      <Container>
        <Container flexDirection="row">
          <Text color={"#fff"} flexGrow={1}>
            Video presets:
          </Text>
          <VRButton
            title={isShowVideosSection ? "^ hide ^" : "v show v"}
            onClick={() => setIsShowVideosSection((old) => !old)}
            type={"primary"}
          />
        </Container>
        <Container
          display={isShowVideosSection ? "flex" : "none"}
          flexDirection="column"
          gapRow={5}
        >
          {videoPresets.map((element, index) => (
            <VRButton
              index={index}
              key={index}
              title={element.name}
              type={"primary"}
              onClick={() => {
                setHFOV(element.hfov);
                setVFOV(element.vfov);
                setVideo(element.href);
              }}
            />
          ))}
        </Container>
      </Container>

      <Container>
        <Container flexDirection="row">
          <Text color={"#fff"} flexGrow={1}>
            Video slots (may be empty):
          </Text>
          <VRButton
            title={isShowVideosSlots ? "^ hide ^" : "v show v"}
            onClick={() => setIsShowVideosSlots((old) => !old)}
            type={"primary"}
          />
        </Container>
        <Container
          display={isShowVideosSlots ? "flex" : "none"}
          flexDirection="column"
          gapRow={5}
        >
          {videoSlots.map((element, index) => (
            <VRButton
              index={index}
              key={index}
              title={element.filename ?? element.title}
              type={"primary"}
              onClick={() => {
                if (element.hfov) setHFOV(element.hfov);
                if (element.vfov) setVFOV(element.vfov);
                setVideo(element.filename);
              }}
            />
          ))}
        </Container>
      </Container>

      <Container flexDirection="column" gapColumn={10}>
        <Container flexDirection="row">
          <Text color={"#fff"} flexGrow={1}>
            Screen settings:
          </Text>
          <VRButton
            title={isShowScreenSettingSection ? "^ hide ^" : "v show v"}
            onClick={() => setIsShowScreenSettingSection((old) => !old)}
            type={"primary"}
          />
        </Container>

        <Container
          display={isShowScreenSettingSection ? "flex" : "none"}
          flexDirection="column"
          gapRow={5}
        >
          <Container flexDirection="column" gapColumn={10}>
            <Text color={"#fff"} flexGrow={1}>
              Screen presets:
            </Text>
            <Container flexDirection="row" gapColumn={4}>
              <VRButton
                title={"120"}
                onClick={() => {
                  setHFOV(120);
                  setVFOV(67.5);
                }}
                type={"primary"}
                width={30}
                flexGrow={1}
              />
              <VRButton
                title={"180"}
                onClick={() => {
                  setHFOV(180);
                  setVFOV(180);
                }}
                type={"primary"}
                width={30}
                flexGrow={1}
              />
              <VRButton
                title={"360"}
                onClick={() => {
                  setHFOV(360);
                  setVFOV(180);
                }}
                type={"primary"}
                width={30}
                flexGrow={1}
              />
            </Container>
          </Container>
          <Container flexDirection="row" gapColumn={10}>
            <Text color={"#fff"} flexGrow={1}>
              Horizontal FOV: {hfov.toFixed(0).toString()}
            </Text>
            <Container flexDirection="row" gapColumn={4}>
              <VRButton
                title={"-"}
                onClick={() => changeHFOV(-step)}
                type={"primary"}
                width={30}
              />
              <VRButton
                title={"+"}
                onClick={() => changeHFOV(step)}
                type={"primary"}
                width={30}
              />
              <VRButton
                title={"Auto"}
                type={"primary"}
                onClick={() => {
                  //16 - 9
                  //x - vfov
                  setHFOV((16 * vfov) / 9);
                }}
              />
            </Container>
          </Container>
          <Container flexDirection="row" gapColumn={10}>
            <Text color={"#fff"} flexGrow={1}>
              Vertical FOV: {vfov.toFixed(0).toString()}
            </Text>
            <Container flexDirection="row" gapColumn={4}>
              <VRButton
                title={"-"}
                onClick={() => changeVFOV(-step)}
                type={"primary"}
                width={30}
              />
              <VRButton
                title={"+"}
                onClick={() => changeVFOV(step)}
                type={"primary"}
                width={30}
              />
              <VRButton
                title={"Auto"}
                type={"primary"}
                onClick={() => {
                  //16 - 9
                  //hfov - x
                  setVFOV((9 * hfov) / 16);
                }}
              />
            </Container>
          </Container>
          <Container flexDirection="row" gapColumn={10}>
            <Text color={"#fff"} flexGrow={1}>
              Radius: {radius.toFixed(1).toString()}
            </Text>
            <Container flexDirection="row" gapColumn={4}>
              <VRButton
                title={"-"}
                onClick={() => changeRadius(-0.5)}
                type={"primary"}
                width={30}
              />
              <VRButton
                title={"+"}
                onClick={() => changeRadius(0.5)}
                type={"primary"}
                width={30}
              />
            </Container>
          </Container>
          <Container flexDirection="row" gapColumn={10}>
            <Text color={"#fff"} flexGrow={1}>
              Screen FOV change step: {step.toString()}
            </Text>
            <Container flexDirection="row" gapColumn={4}>
              <VRButton
                title={"-"}
                onClick={() => setStep((old) => old - 0.5)}
                type={"primary"}
                width={30}
              />
              <VRButton
                title={"+"}
                onClick={() => setStep((old) => old + 0.5)}
                type={"primary"}
                width={30}
              />
            </Container>
          </Container>
        </Container>
      </Container>

      <Container flexDirection="column" gapRow={4}>
        <Container flexDirection="row" gapRow={4}>
          <Text color={"#fff"} flexGrow={1}>
            Translate screen:
          </Text>
          <VRButton
            title={isShowTranslateSection ? "^ hide ^" : "v show v"}
            onClick={() => setIsShowTranslateSection((old) => !old)}
            type={"primary"}
          />
        </Container>
        <Container
          display={isShowTranslateSection ? "flex" : "none"}
          flexDirection="column"
          gapRow={5}
        >
          <Container flexDirection="row" gapColumn={4}>
            <VRButton
              title={"Up"}
              onClick={() => translate(new Vector3(0, +translationStep, 0))}
              type={"primary"}
              width={30}
              flexGrow={1}
            />
            <VRButton
              title={"Down"}
              onClick={() => translate(new Vector3(0, -translationStep, 0))}
              type={"primary"}
              width={30}
              flexGrow={1}
            />
          </Container>
          <Container flexDirection="row" gapColumn={4}>
            <VRButton
              title={"Left"}
              onClick={() => translate(new Vector3(-translationStep, 0, 0))}
              type={"primary"}
              width={30}
              flexGrow={1}
            />
            <VRButton
              title={"Right"}
              onClick={() => translate(new Vector3(translationStep, 0, 0))}
              type={"primary"}
              width={30}
              flexGrow={1}
            />
          </Container>
          <Container flexDirection="row" gapColumn={4}>
            <VRButton
              title={"Forward"}
              onClick={() => translate(new Vector3(0, 0, -translationStep))}
              type={"primary"}
              width={30}
              flexGrow={1}
            />
            <VRButton
              title={"Backward"}
              onClick={() => translate(new Vector3(0, 0, translationStep))}
              type={"primary"}
              width={30}
              flexGrow={1}
            />
          </Container>
        </Container>
      </Container>
      <Container flexDirection="row">
        <VRButton
          flexGrow={1}
          title={`Toggle stereo (now: ${isStereo ? "on" : "off"})`}
          onClick={toggleStereo}
          type={"primary"}
          width={30}
        />
      </Container>
    </VRDialogWindow>
  );
});
