import {
  ContainerStyle,
  SVGStyle,
  TextStyle,
} from '../../commons/vr-utils/vrClassNames';
import theme from '../../theme';

const Style: {
  container: ContainerStyle;
  micIcon: SVGStyle;
  label: TextStyle;
  button: ContainerStyle;
} = {
  container: [
    {
      gapRow: 50,
      paddingY: 60,
      paddingX: 40,
    },
  ],
  micIcon: [
    {
      height: 80,
      color: theme.lightColor,
      marginX: 'auto',
    },
  ],
  label: [
    {
      color: theme.lightColor,
      fontSize: 30,
      horizontalAlign: 'center',
    },
  ],
  button: [
    {
      paddingX: 100,
      marginX: 'auto',
    },
  ],
};
export default Style;
