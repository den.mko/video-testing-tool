import {memo, useMemo} from 'react';
import {SVG, Text} from '@coconut-xr/koestlich';

import styles from './style';
import {VRButton} from '../../commons/button';
import {VRDialogWindow} from '../../commons/dialog';
import {vrClassNames} from '../../commons/vr-utils/vrClassNames';

type Props = {
  onToggleAudioClick: () => void;
};

export const VRTurnOnMicDialog = memo((props: Props) => {
  const {onToggleAudioClick} = props;

  const micStyle = useMemo(() => vrClassNames(styles.micIcon), []);
  const microphoneDisabledText = 'microphone-disabled-popup-in-call-text';
  const microphoneDisabledButtonText =
    'microphone-disabled-popup-in-call-button';

  return (
    <VRDialogWindow type={'primary'} classes={styles.container}>
      <SVG url={'/i/svg/mic-filled-enable.svg'} classes={micStyle} />
      <Text classes={styles.label}>{microphoneDisabledText}</Text>
      <VRButton
        title={microphoneDisabledButtonText}
        type={'light'}
        classes={styles.button}
        onClick={onToggleAudioClick}
      />
    </VRDialogWindow>
  );
});
