import {memo, useState} from 'react';
import {GetMinutesStep} from './getMinutes';
import {ChoosePackageStep} from './choosePackage';
import {TermAndConditions} from './termsAndConditions';

export const FastBuyDialog = memo(() => {
  const [step, setStep] = useState<'intro' | 'choose-package' | 'terms'>(
    'intro',
  );

  if (step === 'intro') return <GetMinutesStep />;
  if (step === 'choose-package') return <ChoosePackageStep />;
  if (step === 'terms') return <TermAndConditions />;
});
