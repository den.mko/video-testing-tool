import {
  ContainerStyle,
  TextStyle,
} from '../../../commons/vr-utils/vrClassNames';
import theme from '../../../theme';

const Style: {
  wrapper: ContainerStyle;
  button: ContainerStyle;
  buttonLabel: TextStyle;
  upperLabelContainer: ContainerStyle;
  upperLabel: TextStyle;
  timerLabel: TextStyle;
  mainLabel: TextStyle;
  disclaimerContainer: ContainerStyle;
  disclaimer: TextStyle;
  disclaimerLink: TextStyle;
} = {
  wrapper: [
    {
      maxWidth: 660,
      width: 660,
    },
  ],
  button: [
    {
      marginY: 20,
    },
  ],
  buttonLabel: [
    {
      fontSize: 24,
      color: '#000',
      fontFamily: 'extrabold',
    },
  ],
  upperLabelContainer: [
    {
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
    },
  ],
  upperLabel: [
    {
      color: theme.lightColor,
      fontFamily: 'semibold',
      fontSize: 24,
    },
  ],
  timerLabel: [
    {
      color: '#E76BD3',
    },
  ],
  mainLabel: [
    {
      color: theme.lightColor,
      fontSize: 28,
      fontFamily: 'extrabold',
      alignSelf: 'center',
    },
  ],
  disclaimerContainer: [
    {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      gapColumn: 5,
      alignItems: 'center',
      justifyContent: 'center',
    },
  ],
  disclaimer: [
    {
      color: '#A8A8A8',
      fontSize: 16,
      fontFamily: 'regular',
      lineHeightMultiplier: 0.9,
    },
  ],
  disclaimerLink: [
    {
      color: '#E76BD3',
      fontSize: 16,
      lineHeightMultiplier: 0.9,
    },
  ],
};
export default Style;
