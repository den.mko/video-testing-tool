import {memo, useEffect, useMemo, useState} from 'react';
import {Container} from '@coconut-xr/koestlich';
import {Text} from '@coconut-xr/koestlich';
import {vrClassNames} from '../../../commons/vr-utils/vrClassNames';
import {VRDialogWindow} from '../../../commons/dialog';
import {VRButton} from '../../../commons/button';
import style from './style';

export const GetMinutesStep = memo(() => {
  const timerStyles = useMemo(
    () => vrClassNames(style.upperLabel, style.timerLabel),
    [],
  );

  const [time, setTime] = useState(60);

  useEffect(() => {
    const timeout = setInterval(() => {
      setTime((it) => {
        if (it === 0) {
          clearInterval(timeout);
          return it;
        }
        return it - 1;
      });
    }, 1000);

    return () => clearInterval(timeout);
  }, []);

  return (
    <VRDialogWindow
      type={'primary'}
      classes={style.wrapper}
      closeButtonType={'light-close'}
      withCloseButton
    >
      <Container classes={style.upperLabelContainer}>
        <Text classes={style.upperLabel}>Video Call will end in </Text>
        <Text classes={timerStyles}>{time.toString()} sec</Text>
      </Container>
      <Text classes={style.mainLabel}>
        Get Video Call Minutes to stay in the Call!
      </Text>
      <VRButton type={'light'} classes={style.button}>
        <Text classes={style.buttonLabel}>GET 26 MINUTES</Text>
      </VRButton>
      <Container classes={style.disclaimerContainer}>
        <Text classes={style.disclaimer}>160 Credits for $49.99 or </Text>
        <Text classes={style.disclaimerLink}>Choose another package.</Text>
        <Text classes={style.disclaimer}>
          Payments processed by SOL Networks Limited (Sliema, Malta).
        </Text>
        <Text classes={style.disclaimer}>By purchasing you accept</Text>
        <Text classes={style.disclaimerLink}>Terms & Conditions</Text>
        <Text classes={style.disclaimer}>and</Text>
        <Text classes={style.disclaimerLink}>Privacy Policy.</Text>
      </Container>
    </VRDialogWindow>
  );
});
