import {
  ContainerStyle,
  TextStyle,
} from '../../../commons/vr-utils/vrClassNames';
import theme from '../../../theme';

const Style: {
  wrapper: ContainerStyle;
  title: TextStyle;
  scrollableContainer: ContainerStyle;
  termsText: TextStyle;
} = {
  wrapper: [
    {
      maxWidth: 660,
      width: 660,
      gapRow: 16,
    },
  ],
  title: [
    {
      fontSize: 18,
      fontFamily: 'semibold',
      alignSelf: 'center',
    },
  ],
  scrollableContainer: [
    {
      height: 250,
      overflow: 'scroll',
    },
  ],
  termsText: [
    {
      fontSize: 16,
    },
  ],
};
export default Style;
