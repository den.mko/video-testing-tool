import {
  Container,
  ContainerNode,
  ExtendedThreeEvent,
  Text,
} from "@coconut-xr/koestlich";
import { memo, useRef, useState } from "react";
import { VRDialogWindow } from "../../../commons/dialog";
import style from "./style";

function censor(censor: any) {
  let i = 0;

  return function (key: any, value: any) {
    if (
      i !== 0 &&
      typeof censor === "object" &&
      typeof value == "object" &&
      censor == value
    )
      return "[Circular]";

    if (i >= 29)
      // seems to be a harded maximum of 30 serialized objects?
      return "[Unknown]";

    ++i; // so we know we aren't using the original object anymore

    return value;
  };
}

export const TermAndConditions = memo(() => {
  const [wheelEvent, setWheelEvent] =
    useState<ExtendedThreeEvent<WheelEvent>>();

  const ccc = useRef<ContainerNode>();
  ccc.current?.onUpdate;

  // ccc.current.scrol

  return (
    <VRDialogWindow type={"light"} classes={style.wrapper} withCloseButton>
      <Text classes={style.title}>Dating.com: Terms of Use Agreement</Text>
      <Container classes={style.scrollableContainer}>
        <Text classes={style.termsText}>
          Last updated October, 2022 This Terms of Use Agreement (this
          «Agreement») is made between SOL Networks Limited, located at 71,
          Tower Road, SLM 1609, Sliema, Malta (hereinafter "The Company", or the
          "Website", or the "Application (including applications for tablets and
          other smart devices)) and the user of Dating.com and its affiliate
          services («You» or «Member») with respect to communications with other
          members and other online services provided by Dating.com (the
          «Service»). By clicking «Sign in with Google», «Take a chance» (if
          applicable) or «Continue with Google» (if applicable) or «Continue»
          (if applicable) or «See Results» (if applicable) on the registration
          forms, You indicate Your acceptance Last updated October, 2022 This
          Terms of Use Agreement (this «Agreement») is made between SOL Networks
          Limited, located at 71, Tower Road, SLM 1609, Sliema, Malta
          (hereinafter "The Company", or the "Website", or the "Application
          (including applications for tablets and other smart devices)) and the
          user of Dating.com and its affiliate services («You» or «Member») with
          respect to communications with other members and other online services
          provided by Dating.com (the «Service»). By clicking «Sign in with
          Google», «Take a chance» (if applicable) or «Continue with Google» (if
          applicable) or «Continue» (if applicable) or «See Results» (if
          applicable) on the registration forms, You indicate Your acceptance
          Last updated October, 2022 This Terms of Use Agreement (this
          «Agreement») is made between SOL Networks Limited, located at 71,
          Tower Road, SLM 1609, Sliema, Malta (hereinafter "The Company", or the
          "Website", or the "Application (including applications for tablets and
          other smart devices)) and the user of Dating.com and its affiliate
          services («You» or «Member») with respect to communications with other
          members and other online services provided by Dating.com (the
          «Service»). By clicking «Sign in with Google», «Take a chance» (if
          applicable) or «Continue with Google» (if applicable) or «Continue»
          (if applicable) or «See Results» (if applicable) on the registration
          forms, You indicate Your acceptance
        </Text>
      </Container>
    </VRDialogWindow>
  );
});
