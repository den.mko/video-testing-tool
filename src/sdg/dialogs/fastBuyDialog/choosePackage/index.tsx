import {memo, useMemo} from 'react';
import {vrClassNames} from '../../../commons/vr-utils/vrClassNames';
import {VRDialogWindow} from '../../../commons/dialog';
import {Container, Text} from '@coconut-xr/koestlich';
import style from './style';
import {VRButton} from '../../../commons/button';

export const ChoosePackageStep = memo(() => {
  const highlightetButton = useMemo(
    () => vrClassNames(style.packageButton, style.highligtetButton),
    [],
  );

  return (
    <VRDialogWindow
      type={'primary'}
      classes={style.wrapper}
      closeButtonType={'light-close'}
    >
      <Text classes={style.title}>Choose the Package:</Text>
      <Container classes={style.buttonsContainer}>
        <VRButton type="light" classes={style.packageButton}>
          <Text classes={style.buttonMinutesLabel}>3 Minuntes</Text>
          <Text classes={style.buttonCreditsLabel}>20 Credits</Text>
        </VRButton>
        <VRButton type="light" classes={highlightetButton}>
          <Text classes={style.buttonMinutesLabel}>26 Minuntes</Text>
          <Text classes={style.buttonCreditsLabel}>160 Credits</Text>
        </VRButton>
        <VRButton type="light" classes={style.packageButton}>
          <Text classes={style.buttonMinutesLabel}>166 Minuntes</Text>
          <Text classes={style.buttonCreditsLabel}>1000 Credits</Text>
        </VRButton>
      </Container>
    </VRDialogWindow>
  );
});
