import {
  ContainerStyle,
  TextStyle,
} from '../../../commons/vr-utils/vrClassNames';
import theme from '../../../theme';

const Style: {
  wrapper: ContainerStyle;
  title: TextStyle;
  buttonsContainer: ContainerStyle;
  packageButton: ContainerStyle;
  highligtetButton: ContainerStyle;
  buttonMinutesLabel: TextStyle;
  buttonCreditsLabel: TextStyle;
} = {
  wrapper: [
    {
      maxWidth: 660,
      width: 660,
      alignItems: 'center',
      gapRow: 24,
      paddingTop: 24,
      paddingBottom: 43,
    },
  ],
  title: [
    {
      fontSize: 18,
      color: theme.lightColor,
      fontFamily: 'semibold',
    },
  ],
  buttonsContainer: [
    {
      flexDirection: 'column',
      gapRow: 12,
    },
  ],
  packageButton: [
    {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
      width: 410,
      paddingLeft: 70,
      paddingRight: 24,
    },
  ],
  highligtetButton: [
    {
      border: 4,
      //--Core-Popular-Member
      borderColor: '#FF8400',
    },
  ],
  buttonMinutesLabel: [
    {
      color: '#000',
      fontSize: 18,
      fontFamily: 'semibold',
      flexGrow: 1,
    },
  ],
  buttonCreditsLabel: [
    {
      color: '#000',
      fontSize: 15,
    },
  ],
};
export default Style;
