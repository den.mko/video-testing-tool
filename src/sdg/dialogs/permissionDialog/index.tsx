import {memo} from 'react';
import {VRButton} from '../../commons/button';
import {VRDialogWindow} from '../../commons/dialog';

type Props = {
  onClose: () => void;
};

export const VRPermissionDialog = memo((props: Props) => {
  const {onClose} = props;

  const permissionsAllowedText = 'permissions-popup-allowed-text';

  return (
    <VRDialogWindow type={'primary'} withCloseButton={true}>
      <VRButton
        title={permissionsAllowedText}
        type={'light'}
        onClick={onClose}
      />
    </VRDialogWindow>
  );
});
