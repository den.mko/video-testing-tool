import {memo, useMemo} from 'react';
import type {MeshProps} from '@react-three/fiber';
import type {VideoTexture} from 'three';
import {PlaneGeometry} from 'three';

type Props = {
  texture: VideoTexture;
  layer: number;
  width: number;
  height: number;
  widthSegments?: number;
  heightSegments?: number;
} & MeshProps;

export const FlatVideoMesh = memo((props: Props) => {
  const {
    texture,
    layer,
    width,
    height,
    widthSegments = 16,
    heightSegments = 16,
    ...otherProps
  } = props;

  const screenPlaneGeometry = useMemo<PlaneGeometry>((): PlaneGeometry => {
    const geometry = new PlaneGeometry(
      width,
      height,
      widthSegments,
      heightSegments,
    );

    const uvMap = geometry.attributes.uv.array;
    for (let i = 0; i < uvMap.length; i += 2) {
      uvMap[i] *= 0.5;

      // Right eye (layer 2) -> right part of video
      if (layer === 2) {
        uvMap[i] += 0.5;
      }
    }

    return geometry;
  }, [width, height, widthSegments, heightSegments, layer]);

  return (
    <mesh layers={layer} {...otherProps}>
      <meshBasicMaterial map={texture} toneMapped={false} />
      <planeGeometry {...screenPlaneGeometry} />
    </mesh>
  );
});
