import { useVideoTexture } from "@react-three/drei";
import type { GroupProps } from "@react-three/fiber";
import { memo } from "react";
import { CurvedVideoMesh } from "./curved-video-mesh";

type Props = {
  mediaStream: MediaStream | string;
  stereo: boolean;
  horizontalFov: number;
  verticalFov: number;
  radius: number;
  widthSegments?: number;
  heightSegments?: number;
} & GroupProps;

export const VRCurvedVideoScreen = memo((props: Props) => {
  const {
    mediaStream,
    stereo,
    horizontalFov,
    verticalFov,
    radius,
    widthSegments,
    heightSegments,
    children,
    ...otherProps
  } = props;

  const texture = useVideoTexture(mediaStream);

  return (
    <group {...otherProps}>
      <CurvedVideoMesh
        texture={texture}
        layer={stereo ? 1 : 0}
        horizontalFov={horizontalFov}
        verticalFov={verticalFov}
        radius={radius}
        widthSegments={widthSegments}
        heightSegments={heightSegments}
      />
      {stereo && (
        <CurvedVideoMesh
          texture={texture}
          layer={2}
          horizontalFov={horizontalFov}
          verticalFov={verticalFov}
          radius={radius}
          widthSegments={widthSegments}
          heightSegments={heightSegments}
        />
      )}
      {children}
    </group>
  );
});
