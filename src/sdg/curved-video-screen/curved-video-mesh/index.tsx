import type { MeshProps } from "@react-three/fiber";
import { memo, useMemo } from "react";
import type { VideoTexture } from "three";
import { BackSide, SphereGeometry } from "three";
import { degToRad } from "../../commons/vr-utils/converters";

type Props = {
  texture: VideoTexture;
  layer: number;
  horizontalFov: number;
  verticalFov: number;
  radius: number;
  widthSegments?: number;
  heightSegments?: number;
} & MeshProps;

export const CurvedVideoMesh = memo((props: Props) => {
  const {
    texture,
    layer,
    horizontalFov,
    verticalFov,
    radius,
    widthSegments = 32,
    heightSegments = 32,
    ...otherProps
  } = props;

  const screenSphereGeometry = useMemo((): SphereGeometry => {
    const fullSphereHorizontalDeg = 540; //360 + 180
    const fullSphereVerticalDeg = 180;

    const phiStart = degToRad((fullSphereHorizontalDeg - horizontalFov) / 2);
    const thetaStart = degToRad((fullSphereVerticalDeg - verticalFov) / 2);
    const phiLength = degToRad(horizontalFov);
    const thetaLength = degToRad(verticalFov);

    const geometry = new SphereGeometry(
      radius,
      widthSegments,
      heightSegments,
      phiStart,
      phiLength,
      thetaStart,
      thetaLength,
    );

    const uvMap = geometry.attributes.uv.array;
    for (let i = 0; i < uvMap.length; i += 2) {
      uvMap[i] *= 0.5;

      // Right eye (layer 2) -> right part of video
      if (layer === 2) {
        uvMap[i] += 0.5;
      }
    }

    return geometry;
  }, [
    heightSegments,
    horizontalFov,
    layer,
    radius,
    verticalFov,
    widthSegments,
  ]);

  return (
    <mesh layers={layer} {...otherProps}>
      <meshBasicMaterial map={texture} toneMapped={false} side={BackSide} />
      <sphereGeometry {...screenSphereGeometry} />
    </mesh>
  );
});
