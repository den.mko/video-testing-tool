import { NonImmersiveCamera } from "@coconut-xr/natuerlich/react";
import { OrbitControls, Sky } from "@react-three/drei";
import { memo, useEffect, useMemo, useState } from "react";
import { Euler, Layers, Vector3 } from "three";
import { VRGrabbableUI } from "./commons/grabbable";
import { calculatePositionOnSphereShellEdge } from "./commons/vr-utils/positioning";
import { VRCurvedVideoScreen } from "./curved-video-screen";
import { FastBuyDialog } from "./dialogs/fastBuyDialog";
import { FOVChangeDialog } from "./dialogs/fovChange";
import { VRNotificationDialog } from "./dialogs/notificationDialog";
import { VRSession } from "./vr-session";
import { VRUIWrapper } from "./vr-ui-wrapper";

const objectPositions = {
  popup: {
    position: new Vector3(1, 1, -2),
  },
  exitButton: {
    rotation: new Euler(0, -Math.PI / 5, 0),
  },
  directionalLight: {
    position: new Vector3(1, 1, 2),
  },
  flatCamera: {
    position: new Vector3(0, 0, 0),
    rotation: new Euler(0, 0, 0),
  },
  curvedScreen: {
    position: new Vector3(0, 0.4, 1.6),
    rotation: new Euler(0, 0, 0),
  },
};

type Props = {
  isStereoByDefault: boolean;
};

export const DemoVRSDGPage = memo(({ isStereoByDefault }: Props) => {
  const [horizontalFov, setHorizontalFov] = useState(80);
  const [verticalFov, setVerticalFov] = useState(60);
  const [screenRadius, setScreenRadius] = useState(5);

  const [isStereo, setStereo] = useState(isStereoByDefault);

  useEffect(() => {
    setStereo(isStereoByDefault);
  }, [isStereoByDefault]);

  const [nonImmersiveCameraLayers] = useState(new Layers());

  useEffect(() => {
    nonImmersiveCameraLayers.enableAll();
  }, [nonImmersiveCameraLayers]);

  const notificationPosition = useMemo<Vector3>((): Vector3 => {
    return calculatePositionOnSphereShellEdge(
      screenRadius - 0.7,
      horizontalFov,
      verticalFov,
      "center",
      "bottom",
    ).add(new Vector3(0, 1, 1));
  }, [horizontalFov, screenRadius, verticalFov]);

  const [videoUrl, setVideoUrl] = useState(
    `/presets/video (kandao).mp4?t=${new Date()}`,
  );
  const [screenPosition, setScreenPosition] = useState(
    objectPositions.curvedScreen.position,
  );

  return (
    <VRSession>
      <Sky
        distance={450000}
        sunPosition={[0, 1, 0]}
        inclination={0}
        azimuth={0.25}
      />
      <directionalLight
        position={objectPositions.directionalLight.position}
        intensity={1.6}
      />
      <VRCurvedVideoScreen
        mediaStream={videoUrl}
        stereo={isStereo}
        horizontalFov={horizontalFov}
        verticalFov={verticalFov}
        radius={screenRadius}
        position={screenPosition}
        rotation={objectPositions.curvedScreen.rotation}
      >
        {false && (
          <VRUIWrapper
            position={notificationPosition}
            anchorX={"center"}
            anchorY={"bottom"}
          >
            <VRNotificationDialog text={"microphoneDeniedText"} />
          </VRUIWrapper>
        )}
      </VRCurvedVideoScreen>

      {false && (
        <VRUIWrapper position={objectPositions.popup.position}>
          <FastBuyDialog />
        </VRUIWrapper>
      )}

      {true && (
        <VRGrabbableUI position={objectPositions.popup.position}>
          <VRUIWrapper anchorX={"right"}>
            <FOVChangeDialog
              setVideo={(link: string) =>
                setVideoUrl(`/${link}?t=${new Date()}`)
              }
              changeHFOV={(step: number) =>
                setHorizontalFov((val) => {
                  const result = val + step;
                  if (result > 360) return 360;
                  if (result < 0) return 0;
                  return result;
                })
              }
              changeVFOV={(step: number) =>
                setVerticalFov((val) => {
                  const result = val + step;
                  if (result > 180) return 180;
                  if (result < 0) return 0;
                  return result;
                })
              }
              changeRadius={(step: number) => {
                setScreenRadius((val) => {
                  const result = val + step;
                  if (result < 0) return 0;
                  return result;
                });
              }}
              translate={(vector: Vector3) =>
                setScreenPosition((old) => vector.add(old))
              }
              setHFOV={setHorizontalFov}
              setVFOV={setVerticalFov}
              toggleStereo={() => setStereo((old) => !old)}
              isStereo={isStereo}
              hfov={horizontalFov}
              vfov={verticalFov}
              radius={screenRadius}
            />
          </VRUIWrapper>
        </VRGrabbableUI>
      )}

      <OrbitControls />
      <NonImmersiveCamera
        position={[0, -0.5, 4]}
        layers={nonImmersiveCameraLayers}
      />
    </VRSession>
  );
});
