import {
  type ComponentProps,
  memo,
  useCallback,
  useState,
  Suspense,
} from 'react';
import type {Properties} from '@react-three/fiber/dist/declarations/src/three-types';
import type {WebGLRenderer} from 'three';
import {
  PointerController,
  TouchHand,
  XRCanvas,
} from '@coconut-xr/natuerlich/defaults';
import {
  ImmersiveSessionOrigin,
  useHeighestAvailableFrameRate,
  useInputSources,
  useNativeFramebufferScaling,
} from '@coconut-xr/natuerlich/react';
import {FontFamilies, FontFamilyProvider} from '@coconut-xr/koestlich';
import {getInputSourceId} from '@coconut-xr/natuerlich';

const options: Partial<Properties<WebGLRenderer>> = {
  localClippingEnabled: true,
};

type Props = ComponentProps<typeof XRCanvas>;

const fontFamilies: FontFamilies = {
  regular: ['https://localpc.denmko.com/fonts/opensans/regular/', 'font.json'], // 400
  semibold: [
    'https://localpc.denmko.com/fonts/opensans/semibold/',
    'font.json',
  ], // 600
  extrabold: [
    'https://localpc.denmko.com/fonts/opensans/extrabold/',
    'font.json',
  ], // 800
};

export const VRSession = memo((props: Props) => {
  // const [inputSourceIdMap, setInputSourceIdMap] = useState<
  //   Map<XRInputSource, number>
  // >(new Map());

  // const getInputSourceId = useCallback(
  //   (input: XRInputSource): number => {
  //     let id = inputSourceIdMap.get(input);

  //     if (!id) {
  //       const updatedMap = new Map(inputSourceIdMap);
  //       id = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
  //       updatedMap.set(input, id);
  //       setInputSourceIdMap(updatedMap);
  //     }

  //     return id;
  //   },
  //   [inputSourceIdMap],
  // );

  const inputSources = useInputSources();
  const frameBufferScaling = useNativeFramebufferScaling();
  const highestAvailableFramerate = useHeighestAvailableFrameRate();

  return (
    <XRCanvas
      dpr={window.devicePixelRatio}
      frameBufferScaling={frameBufferScaling}
      frameRate={highestAvailableFramerate}
      gl={options}
      {...props}
    >
      {/* <FontFamilyProvider
        fontFamilies={fontFamilies}
        defaultFontFamily={'regular'}
      > */}
      {props.children}
      {/* </FontFamilyProvider> */}
      <ImmersiveSessionOrigin>
        {inputSources.map((inputSource) =>
          inputSource.hand != null ? (
            <TouchHand
              cursorOpacity={1}
              key={getInputSourceId(inputSource)}
              id={getInputSourceId(inputSource)}
              inputSource={inputSource}
              hand={inputSource.hand}
            />
          ) : (
            <PointerController
              key={getInputSourceId(inputSource)}
              id={getInputSourceId(inputSource)}
              inputSource={inputSource}
            />
          ),
        )}
      </ImmersiveSessionOrigin>
    </XRCanvas>
  );
});
