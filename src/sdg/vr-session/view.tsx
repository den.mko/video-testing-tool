import type {ComponentProps} from 'react';
import {useCallback, memo} from 'react';
import {
  PointerController,
  TouchHand,
  XRCanvas,
} from '@coconut-xr/natuerlich/defaults';
import {
  ImmersiveSessionOrigin,
  useHeighestAvailableFrameRate,
  useInputSources,
  useNativeFramebufferScaling,
} from '@coconut-xr/natuerlich/react';

type Props = {
  getInputSourceId: (input: XRInputSource) => number;
} & ComponentProps<typeof XRCanvas>;

export default memo((props: Props) => {
  const {getInputSourceId, children, ...otherProps} = props;

  const inputSources = useInputSources();
  const frameBufferScaling = useNativeFramebufferScaling();
  const highestAvailableFramerate = useHeighestAvailableFrameRate();

  const getInputId = useCallback(
    (inputSource: XRInputSource) => {
      return getInputSourceId(inputSource);
    },
    [getInputSourceId],
  );

  return (
    <XRCanvas
      // dpr={window.devicePixelRatio}
      frameBufferScaling={frameBufferScaling}
      frameRate={highestAvailableFramerate}
      {...otherProps}
    >
      {props.children}
      <ImmersiveSessionOrigin>
        {inputSources.map((inputSource) =>
          inputSource.hand != null ? (
            <TouchHand
              cursorOpacity={1}
              key={getInputId(inputSource)}
              id={getInputId(inputSource)}
              inputSource={inputSource}
              hand={inputSource.hand}
            />
          ) : (
            <PointerController
              key={getInputId(inputSource)}
              id={getInputId(inputSource)}
              inputSource={inputSource}
            />
          ),
        )}
      </ImmersiveSessionOrigin>
    </XRCanvas>
  );
});
