import {type ComponentProps, memo, useMemo} from 'react';
import {Container} from '@coconut-xr/koestlich';
import style, {DialogStyleVariant} from './style';
import {VRButton} from '../button';
import {ContainerStyle, vrClassNames} from '../vr-utils/vrClassNames';
import {ButtonStyleVariant} from '../button/style';

type Props = {
  type: DialogStyleVariant;
  closeButtonType?: ButtonStyleVariant;
  withCloseButton?: boolean;
  onClose?: () => void;
} & ComponentProps<typeof Container>;

export const VRDialogWindow = memo((props: Props) => {
  const {
    type,
    onClose,
    children,
    classes,
    closeButtonType,
    withCloseButton = false,
    ...otherProps
  } = props;

  const containerStyle = useMemo<ContainerStyle>(
    () => vrClassNames(style[type].container, classes),
    [classes, type],
  );

  const closeButtonCombinedType = useMemo(
    () => closeButtonType ?? style[type].closeButton?.type ?? 'close',
    [closeButtonType, type],
  );
  const closeButtonStyle = useMemo(
    () => style[type].closeButton?.style,
    [type],
  );

  return (
    <Container classes={containerStyle} {...otherProps}>
      {withCloseButton && (
        <VRButton
          icon={'/i/svg/cross.svg'}
          classes={closeButtonStyle}
          type={closeButtonCombinedType}
        />
      )}
      {children}
    </Container>
  );
});
