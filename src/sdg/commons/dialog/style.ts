import { makeBorderMaterial } from "@coconut-xr/xmaterials";
import { MeshPhongMaterial } from "three";
import theme from "../../theme";
import { ButtonStyleVariant } from "../button/style";
import { ContainerStyle } from "../vr-utils/vrClassNames";

type DialogStyle = {
  container?: ContainerStyle;
  closeButton?: {
    type: ButtonStyleVariant;
    style?: ContainerStyle;
  };
};

const BackgroundMaterial = makeBorderMaterial(MeshPhongMaterial, {
  specular: theme.darkColor,
  shininess: 100,
});

const CommonDialogContainer: ContainerStyle = [
  {
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    padding: 30,
    borderRadius: 40,
  },
];

const CloseButtonPositioning: ContainerStyle = [
  {
    positionType: "absolute",
    positionRight: 20,
    positionTop: 20,
  },
];

export type DialogStyleVariant = "primary" | "light" | "notification";
const Styles: { [key in DialogStyleVariant]: DialogStyle } = {
  primary: {
    container: [
      ...CommonDialogContainer,
      {
        backgroundColor: theme.violetBlackColor,
        backgroundOpacity: 0.8,
        borderColor: theme.violetBlackColor,
        material: BackgroundMaterial,
        maxWidth: 600,
        paddingTop: 0,
        paddingBottom: 0,
      },
    ],
    closeButton: {
      type: "light-close",
      style: [...CloseButtonPositioning],
    },
  },
  notification: {
    container: [
      {
        backgroundColor: theme.violetBlackColor,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        borderRadius: 100,
        flexDirection: "row",
        gapRow: 30,
        paddingY: 5,
        paddingX: 60,
        positionType: "relative",
        maxWidth: 600,
      },
    ],
    closeButton: {
      type: "close",
      style: [...CloseButtonPositioning],
    },
  },
  light: {
    container: [
      ...CommonDialogContainer,
      {
        backgroundColor: theme.lightColor,
        backgroundOpacity: 0.8,
        borderColor: theme.lightColor,
        maxWidth: 600,
      },
    ],
    closeButton: {
      type: "close",
      style: [...CloseButtonPositioning],
    },
  },
};
export default Styles;
