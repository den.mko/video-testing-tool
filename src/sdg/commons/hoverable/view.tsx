import type { ExtendedThreeEvent } from "@coconut-xr/koestlich";
import { Container } from "@coconut-xr/koestlich";
import { memo, useCallback, type ComponentPropsWithoutRef } from "react";

type Props = {
  onPointerEnterEvent: (event: ExtendedThreeEvent<PointerEvent>) => void;
  onPointerLeaveEvent: (event: ExtendedThreeEvent<PointerEvent>) => void;
} & ComponentPropsWithoutRef<typeof Container>;

export default memo((props: Props) => {
  const {
    onPointerEnterEvent,
    onPointerLeaveEvent,
    onPointerEnter,
    onPointerLeave,
    ...otherProps
  } = props;

  const pointerEnterHandler = useCallback(
    (event: ExtendedThreeEvent<PointerEvent>) => {
      onPointerEnterEvent(event);
      if (onPointerEnter) {
        onPointerEnter(event);
      }
    },
    [onPointerEnter, onPointerEnterEvent],
  );

  const pointerLeaveHandler = useCallback(
    (event: ExtendedThreeEvent<PointerEvent>) => {
      onPointerLeaveEvent(event);
      if (onPointerLeave) {
        onPointerLeave(event);
      }
    },
    [onPointerLeaveEvent, onPointerLeave],
  );

  return (
    <Container
      onPointerEnter={pointerEnterHandler}
      onPointerLeave={pointerLeaveHandler}
      {...otherProps}
    />
  );
});
