import type { Container, ExtendedThreeEvent } from "@coconut-xr/koestlich";
import { memo, useCallback, useState, type ComponentProps } from "react";
import View from "./view";

type Props = {
  onHover?: (state: boolean) => void;
  onHoverEnter?: () => void;
  onHoverLeave?: () => void;
} & ComponentProps<typeof Container>;

export const VRHoverableContainer = memo((props: Props) => {
  const { onHover, onHoverEnter, onHoverLeave, ...otherProps } = props;

  const [hoverCount, setHoverCount] = useState(0);

  const onPointerEnter = useCallback(
    (event: ExtendedThreeEvent<PointerEvent>) => {
      event.stopPropagation();

      if (hoverCount === 0) {
        if (onHoverEnter) {
          onHoverEnter();
        }
        if (onHover) {
          onHover(true);
        }
      }
      setHoverCount((counter) => counter + 1);
    },
    [hoverCount, onHover, onHoverEnter],
  );

  const onPointerLeave = useCallback(
    (event: ExtendedThreeEvent<PointerEvent>) => {
      event.stopPropagation();
      if (hoverCount === 1) {
        if (onHoverLeave) {
          onHoverLeave();
        }
        if (onHover) {
          onHover(false);
        }
      }
      setHoverCount((counter) => counter - 1);
    },
    [hoverCount, onHover, onHoverLeave],
  );

  return (
    <View
      onPointerEnterEvent={onPointerEnter}
      onPointerLeaveEvent={onPointerLeave}
      {...otherProps}
    />
  );
});
