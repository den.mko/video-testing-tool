import type {ComponentProps} from 'react';
import type {Container, SVG, Text} from '@coconut-xr/koestlich';

export type ContainerStyle = NonNullable<
  ComponentProps<typeof Container>['classes']
>;
export type SVGStyle = NonNullable<ComponentProps<typeof SVG>['classes']>;
export type TextStyle = NonNullable<ComponentProps<typeof Text>['classes']>;

export type ElementStatesStyles<T> = {
  base: T;
  hover?: T;
};

export function vrClassNames<T>(...args: (T[] | undefined | false)[]): T[] {
  return [
    ...args
      .filter((it) => it !== undefined && it !== false)
      .map((it) => it as T[])
      .reduce((acc, it) => acc.concat(it), []),
  ];
}
