import {Quaternion, Vector3} from 'three';
import {degToRad} from './converters';

export function calculatePositionOnSphereShellEdge(
  radius: number,
  horizontalFov: number,
  verticalFov: number,
  positionHorizontal: 'left' | 'right' | 'center',
  positionVertical: 'top' | 'bottom' | 'center',
): Vector3 {
  const center = new Vector3(0, 0, -radius);

  let horizontalMultiplier = 0;
  if (positionHorizontal === 'left') {
    horizontalMultiplier = 1;
  }
  if (positionHorizontal === 'right') {
    horizontalMultiplier = -1;
  }

  let verticalMultiplier = 0;
  if (positionVertical === 'top') {
    verticalMultiplier = 1;
  }
  if (positionVertical === 'bottom') {
    verticalMultiplier = -1;
  }

  const rotationX = new Quaternion().setFromAxisAngle(
    new Vector3(1, 0, 0),
    degToRad((verticalMultiplier * verticalFov) / 2),
  );

  const rotationY = new Quaternion().setFromAxisAngle(
    new Vector3(0, 1, 0),
    degToRad((horizontalMultiplier * horizontalFov) / 2),
  );

  return center.applyQuaternion(rotationX).applyQuaternion(rotationY);
}
