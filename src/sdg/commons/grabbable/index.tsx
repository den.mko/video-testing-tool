import {Grabbable} from '@coconut-xr/natuerlich/defaults';
import {GroupProps, useFrame} from '@react-three/fiber';
import {memo, useCallback, useRef, useState} from 'react';
import {Mesh, Group, Vector3} from 'three';

type Props = {
  position?: Vector3;
} & GroupProps;

export const VRGrabbableUI = memo((props: Props) => {
  const {children, position, ...otherProps} = props;

  const grabbableRef = useRef<Mesh>(null);
  const uiRef = useRef<Group>(null);

  const [isGrabbed, setIsGrabbed] = useState(false);

  const releaseHandler = useCallback(() => {
    setIsGrabbed(false);
    if (uiRef.current) {
      grabbableRef.current?.quaternion.copy(uiRef.current.quaternion);
      grabbableRef.current?.position.copy(uiRef.current.position);
    }
  }, []);

  useFrame(() => {
    if (isGrabbed && grabbableRef.current) {
      const origPos = position ?? new Vector3(0, 0, 0);

      uiRef.current?.quaternion.copy(grabbableRef.current.quaternion);
      uiRef.current?.position.copy(grabbableRef.current.position.sub(origPos));
    }
  });

  return (
    <group position={position} {...otherProps}>
      <Grabbable
        ref={grabbableRef}
        onGrabbed={() => setIsGrabbed(true)}
        onReleased={() => releaseHandler()}
      >
        <mesh position={[0.05, 0, 0]} visible={!isGrabbed}>
          <boxGeometry args={[0.1, 0.5, 0.1]} />
          <meshBasicMaterial color={'#AAA'} />
        </mesh>
      </Grabbable>
      <group ref={uiRef}>{children}</group>
    </group>
  );
});
