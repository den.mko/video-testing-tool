import theme from '../../theme';
import {
  ContainerStyle,
  ElementStatesStyles,
  SVGStyle,
  TextStyle,
} from '../vr-utils/vrClassNames';

type ButtonStyle = {
  container?: ContainerStyle;
  icon?: SVGStyle;
  text?: TextStyle;
};

const CommonButtonContainer: ContainerStyle = [
  {
    flexDirection: 'row',
    gapColumn: 15,
    justifyContent: 'center',
    alignContent: 'center',
    padding: 20,
    borderRadius: 12,
  },
];

export type ButtonStyleVariant = 'light' | 'primary' | 'close' | 'light-close';
const Styles: {[key in ButtonStyleVariant]: ElementStatesStyles<ButtonStyle>} =
  {
    light: {
      base: {
        container: [
          ...CommonButtonContainer,
          {
            backgroundColor: '#fff',
            paddingX: 10,
            paddingTop: 12,
            paddingBottom: 16,
            justifyContent: 'center',
            alignItems: 'center',
          },
        ],
        text: [
          {
            fontSize: 30,
            color: '#000',
          },
        ],
        icon: [
          {
            color: '#000',
            height: 30,
            alignSelf: 'center',
          },
        ],
      },
      hover: {
        container: [
          {
            backgroundColor: theme.vrDarkHoverColor,
          },
        ],
      },
    },
    primary: {
      base: {
        container: [
          ...CommonButtonContainer,
          {
            gapColumn: 5,
            backgroundColor: theme.vrSecondAccentColor,
            paddingX: 10,
            paddingY: 5,
          },
        ],
        text: [
          {
            fontSize: 15,
            color: '#fff',
            alignSelf: 'center',
          },
        ],
        icon: [
          {
            color: '#fff',
            height: 14,
            alignSelf: 'center',
          },
        ],
      },
      hover: {
        container: [
          {
            backgroundColor: theme.violetBlackColor,
          },
        ],
      },
    },
    close: {
      base: {
        container: [
          {
            borderRadius: 100,
            backgroundColor: '#fff',
            border: 1,
            borderColor: theme.actionGrayColor,
            padding: 5,
            height: 25,
            width: 25,
            alignItems: 'center',
            justifyContent: 'center',
          },
        ],
        icon: [
          {
            height: 10,
            color: theme.actionGrayColor,
          },
        ],
      },
    },
    'light-close': {
      base: {
        container: [
          {
            borderRadius: 100,
            padding: 5,
            height: 25,
            width: 25,
            alignItems: 'center',
            justifyContent: 'center',
          },
        ],
        icon: [
          {
            height: 10,
            color: theme.lightColor,
          },
        ],
      },
      hover: {
        container: [
          {
            backgroundColor: '#A8A8A8',
          },
        ],
      },
    },
    // 'notification-close': {
    //   base: [
    //     conta
    //   ]
    // }
  };
export default Styles;
