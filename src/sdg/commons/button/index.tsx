import { SVG, Text } from "@coconut-xr/koestlich";
import type { ComponentProps, ComponentPropsWithoutRef } from "react";
import { memo, useMemo, useState } from "react";
import { VRHoverableContainer } from "../hoverable";
import { ContainerStyle, vrClassNames } from "../vr-utils/vrClassNames";
import type { ButtonStyleVariant } from "./style";
import style from "./style";

type Props = {
  title?: string | string[];
  icon?: string;
  type: ButtonStyleVariant;
  titleProps?: ComponentProps<typeof Text>;
  iconProps?: Omit<ComponentProps<typeof SVG>, "url">;
} & Omit<ComponentPropsWithoutRef<typeof VRHoverableContainer>, "onHover">;

export const VRButton = memo((props: Props) => {
  const {
    title,
    icon,
    type,
    titleProps,
    iconProps,
    onClick,
    children,
    classes = [],
    ...otherProps
  } = props;

  const [isHover, setIsHover] = useState(false);

  const containerStyle = useMemo<ContainerStyle>(
    () =>
      vrClassNames(
        style[type].base.container,
        classes,
        isHover && style[type].hover?.container,
      ),
    [classes, isHover, type],
  );

  const iconStyle = useMemo(
    () =>
      vrClassNames(style[type].base.icon, isHover && style[type].hover?.icon),
    [isHover, type],
  );

  const textStyle = useMemo(
    () =>
      vrClassNames(style[type].base.text, isHover && style[type].hover?.text),
    [isHover, type],
  );

  return (
    <VRHoverableContainer
      onHover={setIsHover}
      onClick={onClick}
      classes={containerStyle}
      {...otherProps}
    >
      {icon && <SVG index={0} url={icon} classes={iconStyle} {...iconProps} />}
      {title && (
        <Text index={1} classes={textStyle} {...titleProps}>
          {title}
        </Text>
      )}
      {children}
      {/* {children && <Container index={2}>{children}</Container>} */}
    </VRHoverableContainer>
  );
});
