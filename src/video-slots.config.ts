type VideoSlot = {
  filename: string;

  vfov?: number;
  hfov?: number;
  title?: string;
};

export const videoSlots: VideoSlot[] = [
  {
    filename: "videos/video1.mp4",
    title: "Test name",
  },
  {
    filename: "videos/video2.mp4",
  },
  {
    filename: "videos/video3.mp4",
  },
  {
    filename: "videos/video4.mp4",
  },
  {
    filename: "videos/video5.mp4",
  },
  {
    filename: "videos/video6.mp4",
  },
  {
    filename: "videos/video1.mkv",
  },
  {
    filename: "videos/video2.mkv",
  },
  {
    filename: "videos/video3.mkv",
  },
  {
    filename: "videos/video4.mkv",
  },
  {
    filename: "videos/video5.mkv",
  },
  {
    filename: "videos/video6.mkv",
  },
];
