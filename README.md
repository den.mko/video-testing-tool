# Video testing tool

## Установка и запуск

`npm i`\
`npm run dev`\
С очков заходить по ссылке https.\
При ошибке ssl в браузере в очках: Advanced -> procees to url (unsafe)

## Тестовые видео

Копировать с заменой в папку public/videos. Доступны слоты `video1.mp4 - video6.mp4` и `video1.mkv - video6.mkv`.

Добавлять еще слоты под видео и менять конфиг (в т.ч. добавлять имена) в `src/video-slots.config.ts`

## Видео пресеты

Скачать с google диска, положить в `public/presets`
